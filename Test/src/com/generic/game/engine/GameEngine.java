package com.generic.game.engine;

import com.generic.game.entities.Player;

public class GameEngine {
	private static GameEngine INSTANCE = null;
	private GameBox gameBox;
	
	protected GameEngine() {
		
	}
	
	public static GameEngine getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new GameEngine();
		}
		return INSTANCE;
	}
	
	public void createGameBox() {
		this.gameBox = this.new GameBox();		
	}
	
	public GameBox getGameBox() {
		return this.gameBox;
	}
	
	public class GameBox {
		Player player;
		
		public void createPlayer() {
			this.player = new Player();
		}
		
		public Player getPlayer() {
			return this.player;
		}
	}
}

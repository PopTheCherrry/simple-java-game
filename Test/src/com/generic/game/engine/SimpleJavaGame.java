package com.generic.game.engine;

import com.generic.game.entities.Player;

public class SimpleJavaGame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GameEngine gameEngine = GameEngine.getInstance();
		gameEngine.createGameBox();
		gameEngine.getGameBox().createPlayer();
		Player player = gameEngine.getGameBox().getPlayer();
		player.setEntityName("Max");
		System.out.println("Hi, I'm "+player.getEntityName()+", I'm the first Player");
		
	}

}

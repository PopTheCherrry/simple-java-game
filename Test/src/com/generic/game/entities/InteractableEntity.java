package com.generic.game.entities;

public abstract class InteractableEntity extends Entity{
	private long hp;
	private long damage;
	
	public long getHp() {
		return hp;
	}
	public void setHp(long hp) {
		this.hp = hp;
	}
	public long getDamage() {
		return damage;
	}
	public void setDamage(long damage) {
		this.damage = damage;
	}

	
}

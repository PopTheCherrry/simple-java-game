package com.generic.game.entities;

public class Player extends InteractableEntity{
	private long level;
	private long exp;
	
	public long getLevel() {
		return level;
	}
	public void setLevel(long level) {
		this.level = level;
	}
	public long getExp() {
		return exp;
	}
	public void setExp(long exp) {
		this.exp = exp;
	}
}
